#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import pandas as pd
from datetime import datetime


def data_preprocessing(refined_data):
    print(datetime.now(), " - Start of Data Preprocessing.")
    orders = refined_data["orders"]
    machines = refined_data["looms"]
    workers = refined_data["workGroupsPerShift"]

    # Start Date

    startDate = refined_data["startDate"]
    startDate = datetime.strptime(startDate, '%m/%d/%Y %H:%M:%S')

    # print(f"Start date received is {startDate}.")
    # Start Date but on 00:00:00 (day calculation has to be like this in order to keep daily setup times correct)

    dayStart = refined_data["startDate"]
    dayStart = dayStart.split(' ')[0] + " 00:00:00"
    dayStart = datetime.strptime(dayStart, '%m/%d/%Y %H:%M:%S')
    extraLoad = (startDate - dayStart).total_seconds() / 60
    # print(f"Extra load since start of the day is {extraLoad} minutes.")
    # print (extraLoad)
    looms_to_remove = []
    # convert maintenance Start and End to minutes and add maintenance Duration, initial machine load and last order on loom

    for k in machines:
        if k["maintenanceStart"] is not None and k["maintenanceEnd"] is not None and k["maintenanceStart"] != "Now":
            k["maintenanceStart"] = datetime.strptime(k["maintenanceStart"], '%m/%d/%Y %H:%M:%S')
            c = k["maintenanceStart"] - dayStart
            k["maintenanceStart"] = c.total_seconds() / 60
            k["maintenanceEnd"] = datetime.strptime(k["maintenanceEnd"], '%m/%d/%Y %H:%M:%S')
            c = k["maintenanceEnd"] - dayStart
            k["maintenanceEnd"] = c.total_seconds() / 60
            k["maintenanceDuration"] = k["maintenanceEnd"] - k["maintenanceStart"]
        elif k['maintenanceStart'] == "Now":
            if k['maintenanceEnd'] is not None:
                k["maintenanceStart"] = extraLoad
                k["maintenanceEnd"] = datetime.strptime(k["maintenanceEnd"], '%m/%d/%Y %H:%M:%S')
                c = k["maintenanceEnd"] - dayStart
                k["maintenanceEnd"] = c.total_seconds() / 60
                k["maintenanceDuration"] = k["maintenanceEnd"] - k["maintenanceStart"]
            else:
                k['maintenanceDuration'] = math.inf
                looms_to_remove.append(k['loomID'])
        else:
            k["maintenanceStart"] = None
            k["maintenanceEnd"] = None
            k["maintenanceDuration"] = 0
        k["currentLoad"] = extraLoad + k["maintenanceDuration"]
        k["lastOrder"] = "Empty"
        # print(f"Maintenance Data and current Load calculated for loomID {k['loomID']}.")

    # Calculate deliveryDate in minutes, add chainType

    for order in orders:
        # order["deliveryDate"] = order["deliveryDate"].replace(",", "")
        order["deliveryDate"] = order["deliveryDate"] + " 23:59:59"
        order["deliveryDate"] = datetime.strptime(order["deliveryDate"], '%m/%d/%Y %H:%M:%S')
        c = order["deliveryDate"] - dayStart
        order["deliveryDate"] = c.total_seconds() / 60
        order["chainType"] = str(order["chainID"]) + (order["type"])
        # print(f"Delivery date calculated for chainID {order['chainID']}.")
    pd.set_option("display.max_rows", None, "display.max_columns", None)
    orders_df = pd.DataFrame.from_dict(refined_data["orders"])
    looms_df = pd.DataFrame.from_dict(refined_data["looms"])

    for m in looms_df.loomID.values:
        if m in looms_to_remove:
            looms_df = looms_df.drop(looms_df[looms_df.loomID == m].index)
    # reconstruct ORDI orders
    looms_df = looms_df.set_index('loomID', drop = False)
    order_columns = orders_df.columns
    orders_df = pd.concat([orders_df.query('status != "ORDI"'),
                           orders_df.query('status == "ORDI"').groupby('chainType', as_index=False)[order_columns]
                          .agg({'status': 'last', 'chainID': 'last', 'deliveryDate': 'last', 'type': 'last',
                                'priorityWeight': 'last', 'fabricType': 'last',
                                'ca': 'last', 'cc': 'last', 'strokesPerMt': 'last', 'yarns': 'last',
                                'drawing': 'last',
                                'variant': 'last', 'incom': 'last', 'comb': 'last', 'combHeight': 'last', 'ybf': 'last',
                                'targetMeters': 'sum'})]).reset_index(drop=True)

    # calculate metersLeft, loomEfficiency, currentLoad and lastOrder

    for i in range(len(orders_df)):
        if orders_df.loc[i, "incom"] <= 6:
            orders_df.loc[i,"loomEfficiency"] = 0.6
        elif orders_df.loc[i, "incom"] <= 14:
            orders_df.loc[i,"loomEfficiency"] = 0.5
        else:
            orders_df.loc[i,"loomEfficiency"] = 0.4
        # print(f"Loom Efficiency calculated for chainID {orders_df.loc[i,'chainID']}.")
        if orders_df.loc[i, "status"] == "ATEL" and orders_df.loc[i, "loomID"] not in looms_to_remove:
            loom_speed = looms_df.loc[int(orders_df.loc[i, "loomID"]), "loomSpeed"]
            '''
            orders_df.loc[i, "metersLeft"] = orders_df.loc[i, "targetMeters"] - (
                    orders_df.loc[i, "kStrokes"] * 1000 / orders_df.loc[i, "strokesPerMt"])
            

            '''
            # add calculation with atelStartDate
            total_time = orders_df.loc[i, "targetMeters"] * orders_df.loc[i, "strokesPerMt"] * orders_df.loc[
                i, 'ybf'] / (
                                 orders_df.loc[i, "loomEfficiency"] * loom_speed)
            orders_df.loc[i,'atelStartDatestr'] = orders_df.loc[i, 'atelStartDate']
            orders_df.loc[i, 'atelStartDate'] = datetime.strptime(orders_df.loc[i, 'atelStartDate'],
                                                                  '%m/%d/%Y %H:%M:%S')
            c = orders_df.loc[i, 'atelStartDate'] - startDate
            orders_df.loc[i, 'atelStartDate'] = c.total_seconds() / 60
            finish_date = orders_df.loc[i, 'atelStartDate'] + total_time

            if finish_date < 0:
                orders_df.loc[i, 'metersLeft'] = 0
            else:

                orders_df.loc[i, 'metersLeft'] = (finish_date) * loom_speed * orders_df.loc[i,'loomEfficiency'] / (orders_df.loc[i,'ybf']*orders_df.loc[i,'strokesPerMt'])

                looms_df.loc[looms_df.loomID == orders_df.loc[i, "loomID"], "currentLoad"] += orders_df.loc[
                                                                                                  i, "metersLeft"] * \
                                                                                              orders_df.loc[
                                                                                                  i, "strokesPerMt"] * \
                                                                                              orders_df.loc[
                                                                                                  i, 'ybf'] / (
                                                                                                      orders_df.loc[
                                                                                                          i, "loomEfficiency"] *
                                                                                                      looms_df.loc[
                                                                                                          looms_df.loomID ==
                                                                                                          orders_df.loc[
                                                                                                              i, "loomID"], "loomSpeed"])

            looms_df.loc[looms_df.loomID == orders_df.loc[i, "loomID"], "lastOrder"] = orders_df.loc[i, "chainType"]
            looms_df.loc[looms_df.loomID == orders_df.loc[
                i, "loomID"], "withATEL"] = 1  # if atel on loom - will be used for loom sequence in the end

        else:
            orders_df.loc[i, "metersLeft"] = orders_df.loc[i, "targetMeters"]
        if orders_df.loc[i,'status'] == "ATEL" and orders_df.loc[i,'loomID'] in looms_to_remove:
            orders_df.loc[i,'status'] = "ORDI"
    atel_orders = orders_df[orders_df.status == 'ATEL']  # keep ATEL orders
    orders_df = orders_df[orders_df.status != 'ATEL']
    looms_df['withATEL'].fillna(0, inplace=True)

    orders_df = orders_df.set_index('chainType')
    atel_orders = atel_orders.set_index('loomID')
    print(datetime.now(), " - End of Data Preprocessing.")
    return looms_df, orders_df, extraLoad, dayStart, workers, atel_orders, dayStart
