#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import data_simple_preprocessing
import json
# import pandas as pd
import datetime
import data_simple_verification
import time
import optimizer
import pika
import sys
import setup_times_calc
import last_orders_calc

# import Json_Input
# import random
# import traceback
# import logging


# if __name__ == "__main__":
#    result = opt(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])

online = sys.argv[1]
host = sys.argv[2]
port = sys.argv[3]
username = sys.argv[4]
password = sys.argv[5]

credentials = pika.PlainCredentials(username, password)
params = pika.ConnectionParameters(host, port, '/', credentials, heartbeat=1860, blocked_connection_timeout=930)
connection = pika.BlockingConnection(params)
channel = connection.channel()


def callback(ch, method, properties, body):
    print(datetime.datetime.now(), " - Algorithm Start.")
    # print(" [x] Received %r" % body.decode())

    input = json.loads(body)
    print(datetime.datetime.now(), " - Data Received Successfully.")
    uuid = input['uuid']
    print(f"Process UUID: {input['uuid']}")
    print(input['data'])
    input_data = input['data']
    # print(f["uuid"])

    # data validation
    problems = {}
    problems['data'] = {}
    try:
        data_ok, problems['data']['message'] = data_simple_verification.data_verification(input_data)

        if data_ok:
            # try:
            # data preprocessing
            first_day_setup = input_data['currentTotalSetupTime']
            looms_df, orders_df, extraLoad, zero_point, number_of_workers, atel_orders, start_date = data_simple_preprocessing.data_preprocessing(
                input_data)

            # find existing orders on looms - verify for ATEL

            last_orders, looms_df = last_orders_calc.last_orders_calculation(looms_df, atel_orders)

            # setup times

            setup_times = setup_times_calc.setup_times_calculator(orders_df, looms_df, last_orders)

            # optimizer

            solutions_dict = optimizer.optimizer_no_splitting(orders_df, looms_df, setup_times, last_orders,
                                                              number_of_workers, first_day_setup, atel_orders,
                                                              extraLoad, start_date)  # number of workers

            solutions_dict['data']['objectiveValues']['makespan'] = str(start_date + datetime.timedelta(
                minutes=round(solutions_dict['data']['objectiveValues']['makespan'], 0)))

            # output

            for name in solutions_dict.keys():
                for order in solutions_dict[name]['orders']:
                    for k in ['setupStartTime', 'setupEndTime', 'processStartTime', 'processEndTime',
                              'deliveryDate']:
                        try:
                            # print (order[k])
                            time_change = datetime.timedelta(minutes=order[k])
                            if k == 'deliveryDate':
                                order[k] = (zero_point + time_change).strftime("%m/%d/%Y")
                            else:
                                order[k] = (zero_point + time_change).strftime("%m/%d/%Y %H:%M:%S")
                        except TypeError:
                            break
            solutions_dict['uuid'] = uuid
            solutions_dict['produced_at'] = int(time.time() * 1000)
            # print(solutions_dict['makespan'])
            output = json.dumps(solutions_dict)
            print(f"{datetime.datetime.now()}  - Algorithm End.")
            channel.basic_publish(exchange='opt-result', routing_key='weaving-scheduling', body=output)

            # except Exception as e:
            #     print(f"{datetime.datetime.now()}  - Algorithm killed - an error occurred, check your data.")
            #     exceptions['uuid'] = uuid
            #     exceptions['produced_at'] = int(time.time() * 1000)
            #     exceptions['data']["Error"] = str(e)
            #     output = json.dumps(exceptions)
            #     raise Exception(output)
            #     # channel.basic_publish(exchange='opt-result', routing_key='weaving-scheduling', body=output)
            #     print(f"{datetime.datetime.now()}  - Algorithm End.")

        else:
            print(f"{datetime.datetime.now()}  - Incorrect Input - data failed validation stage.")
            problems['uuid'] = uuid
            problems['produced_at'] = int(time.time() * 1000)
            output = json.dumps(problems)
            channel.basic_publish(exchange='opt-result', routing_key='weaving-scheduling', body=output)
            print(f"{datetime.datetime.now()}  - Algorithm End.")

    except Exception as e:
        print(f"{datetime.datetime.now()}  - Algorithm killed - an error occurred, check your data.")
        print(str(e))
        error_message = {"message": str(e)}
        error_response = {"uuid": uuid, "data": error_message, "produced_at": int(time.time() * 1000)}
        error_response = json.dumps(error_response)
        channel.basic_publish(exchange='opt-result', routing_key='weaving-scheduling', body=error_response)
        print(f"{datetime.datetime.now()}  - Algorithm End.")


channel.basic_consume(queue='weaving-scheduling_job', on_message_callback=callback, auto_ack=True)
channel.start_consuming()
