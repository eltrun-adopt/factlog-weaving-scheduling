#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import numpy as np
import pandas as pd
import datetime
import json


def solution_check(loom_sequence, orders, best, solutions_dict, looms, atel_orders):
    data = {}
    # print(large_loom_sequence)
    # print(small_loom_sequence)
    data['orders'] = orders
    for k in data['orders']:
        if str(k['chainID']) + str(k['type']) in list(atel_orders.chainType):
            k['chainID'] = "ATEL-" + str(k["chainID"])
    completionTimes = 0
    makespan = 0
    tardiness = 0
    energyCons = 0
    count_parts = {}
    chain_tardiness = {}
    chain_completion_times = {}
    for order in data['orders']:
        # print(order)
        chainType = str(order['chainID']) + str(order['type'])
        if chainType in count_parts.keys():
            count_parts[chainType] += 1
            if chain_tardiness[chainType] < order['tardiness']:
                chain_tardiness[chainType] = order['tardiness']
            if chain_completion_times[chainType] > order['processEndTime']:
                chain_completion_times[chainType] = order['processEndTime']
        else:
            count_parts[chainType] = 1
            chain_tardiness[chainType] = order['tardiness']
            chain_completion_times[chainType] = order['processEndTime']
        if order['processEndTime'] > makespan:
            makespan = order['processEndTime']
        energyCons += order['energyCons']

    for order in data['orders']:
        chainType = str(order['chainID']) + str(order['type'])
        if count_parts[chainType] < 10:
            order['partID'] = str(order['chainID']) + '0' + str(count_parts[chainType]) + order['type']
        else:
            order['partID'] = str(order['chainID']) + str(count_parts[chainType]) + order['type']
        count_parts[chainType] -= 1

    total_loom_sequence = {}
    sequence = {}
    for m in loom_sequence.keys():
        sequence[m] = ''
        if looms.loc[m] == 1:
            for order in data['orders']:
                if order["loomID"] == m and ("ATEL" in str(order["chainID"])):
                    sequence[m] = str(order['partID']) + ";"
            for next_order in loom_sequence[m][1:]:
                for order in data['orders']:
                    if next_order == str(order["chainID"]) + str(order["type"]) and order["loomID"] == m:
                        sequence[m] += str(order["partID"]) + ";"
        else:
            for next_order in loom_sequence[m]:
                for order in data['orders']:
                    if next_order == str(order["chainID"]) + str(order["type"]) and order["loomID"] == m:
                        sequence[m] += str(order["partID"]) + ";"
    data["looms'OrdersSequence"] = sequence
    for k in chain_tardiness.keys():
        tardiness += chain_tardiness[k]
        completionTimes += chain_completion_times[k]
    data['objectiveValues'] = {'makespan': makespan, 'totalTardiness': round(tardiness, 2),
                               'totalEnergyCons': round(energyCons, 3),
                               'totalCompletionTimes': round(completionTimes,3)}

    if makespan < best['data']:
        print(f"{datetime.datetime.now()}  - Solution improved from {best['data']} to {makespan}")
        solutions_dict['data'] = data
        best['data'] = makespan

    return solutions_dict, best
