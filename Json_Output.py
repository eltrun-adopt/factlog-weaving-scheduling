#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import json
import random
import datetime
import numpy as np


def delivery_date_func():
    month = random.randint(6, 7)  # days of year
    day = random.randint(1, 30)
    hour = random.randint(0, 23)
    minute = random.randint(0, 59)
    return month, day, hour, minute


data = {}
# data['general_info'] = []
data['orders'] = []
data['loomsOrderSequence'] = []
data['objectiveValues'] = []

number_of_looms = 2  # number of looms
number_of_chains = 3  # number of unique chains
number_of_fabrics = 2  # number of fabric codes

makespan = 0
totalTardiness = 0
totalEnergyCons = 0
totalCompletionTimes = 0
'''
general_info = {}
general_info["schedule_start"] = "01/06/2021 06:00:00"
general_info['schedule_end'] = "07/06/2021 06:00:00"
general_info["week_workdays"] = 6
'''
# data["general_info"] ={"schedule_start": "06/01/2021 06:00:00", 'schedule_end': "06/07/2021 06:00:00", "week_workdays" : 6}

# data["general_info"].append(general_info)
start_date = datetime.datetime(2021, 6, 1, 6, 0, 0)

# looms

loom_id = random.sample(range(1, 100), number_of_looms)  # create random loom_id

print(loom_id)
# chains

type = ["P", "C", "F"]

chains = []
chain_id = random.sample(range(1, 100), number_of_chains)

for i in chain_id:
    month, day, hour, minute = delivery_date_func()
    # Chain_id
    # new_order = {}
    new_chain = []
    new_chain.append(i)
    new_chain.append(random.randint(1, 10))  # priority_weight
    new_chain.append(random.choice(type))  # type
    new_chain.append(datetime.datetime(2021, month, day, 23, 59, 59))  # delivery date
    chains.append(new_chain)
print(chains)

# orders

orders = []
setup_time = [0, 240, 360, 480]

machine_load = {}
loom_sequence = {}
for i in loom_id:
    machine_load[int(i)] = 0
    loom_sequence[int(i)] = ''
print(loom_sequence)
for i in range(number_of_chains):
    assign_to_looms = loom_id.copy()
    new_order = chains[i].copy()
    number_of_parts = random.randint(1, 3)  # number of parts
    for k in range(1, number_of_parts + 1):
        new_order2 = new_order.copy()
        new_order2.append(random.choice(setup_time))  # setup time -4
        new_order2.append(random.randint(200, 4000))  # process time -5
        new_order2.append(int(min(machine_load, key=machine_load.get)))  # loom_id -6
        new_order2.append(machine_load[new_order2[6]])  # setup start -7
        new_order2.append(new_order2[7] + new_order2[4] + new_order2[5])  # process end -8
        machine_load[new_order2[6]] += new_order2[4] + new_order2[5]
        new_order2.append(random.randint(5, 200))  # target_meters -9
        new_order2.append(new_order2[4] + new_order2[7])  # setup end -10
        new_order2.append(new_order2[10])  # process start -11
        new_order2.append(random.randint(10, 100))  # energy consumption -12
        new_order2.append(str(new_order2[0]) + "0" + str(k))  # partID -  13
        orders.append(new_order2)
        print(new_order2)
# print(orders)
for order in orders:
    new_order = {}

    new_order["chainID"] = order[0]

    new_order["partID"] = int(order[13])
    new_order["type"] = order[2]
    new_order["deliveryDate"] = order[3].strftime("%m/%d/%Y")

    new_order["loomID"] = order[6]
    new_order["setupTime"] = order[4]
    new_order["setupStartTime"] = (start_date + datetime.timedelta(minutes=order[7])).strftime('%m/%d/%Y %H:%M:%S')
    new_order["setupEndTime"] = (start_date + datetime.timedelta(minutes=order[10])).strftime('%m/%d/%Y %H:%M:%S')
    new_order["processStartTime"] = (start_date + datetime.timedelta(minutes=order[11])).strftime('%m/%d/%Y %H:%M:%S')
    if order[8] > makespan:
        makespan = order[8]
    totalEnergyCons += order[12]
    totalCompletionTimes += order[8]
    new_order["processEndTime"] = (start_date + datetime.timedelta(minutes=order[8])).strftime('%m/%d/%Y %H:%M:%S')
    new_order["processingTime"] = order[5]
    new_order["energyConsumption"] = order[12]
    new_order["targetMeters"] = order[9]
    process_end_date = datetime.datetime.strptime(new_order["processEndTime"], '%m/%d/%Y %H:%M:%S')
    new_order["tardiness"] = max((process_end_date - order[3]).total_seconds() / 60.0, 0)
    totalTardiness += new_order["tardiness"]
    new_order["priorityWeight"] = order[1]
    # print(new_order)
    data["orders"].append(new_order)
    # print(order)
    loom_sequence[order[6]] = loom_sequence[order[6]] + str(order[13]) + ';'
#print (loom_sequence)
data['loomsOrderSequence']= loom_sequence
print (data['orders'])
# data['objectiveValues'].append(objective_values)

data['objectiveValues'] = {"makespan": makespan, "totalTardiness": totalTardiness, "totalEnergyCons": totalEnergyCons,
                           "totalCompletionTimes": totalCompletionTimes}
#print(data['objectiveValues'])


with open('t.v2.json', 'w') as outfile:
    json.dump(data, outfile)