#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import pandas as pd
from datetime import datetime


def setup_times_calculator(new_orders, machines, last_orders):
    print(datetime.now(), " - Start of setup times calculation.")
    setup_dict = {}
    setup_keys = list(machines.index)
    for m in setup_keys:
        order_col = list(new_orders.index)
        lastOrder = str(machines.loc[m, "lastOrder"])
        order_row = list(new_orders.index)
        order_row.append(lastOrder)
        setup_times = pd.DataFrame(columns=order_col, index=order_row)
        for i in order_row:
            for j in order_col:
                if i == j:
                    setup_times.loc[i, j] = 0
                elif i in order_col:
                    setup_times.loc[i, j] = knotting(new_orders.loc[j, "ca"], new_orders.loc[j, "cc"],
                                                     new_orders.loc[j, "yarns"], new_orders.loc[j, "comb"],
                                                     new_orders.loc[j, "combHeight"], new_orders.loc[i, "ca"],
                                                     new_orders.loc[i, "cc"],
                                                     new_orders.loc[i, "yarns"], new_orders.loc[i, "comb"],
                                                     new_orders.loc[i, "combHeight"])
                elif i not in order_col:  # last order setup
                    if not math.isnan(last_orders.loc[m,'chainID']):
                        setup_times.loc[i, j] = knotting(new_orders.loc[j, "ca"], new_orders.loc[j, "cc"],
                                                         new_orders.loc[j, "yarns"], new_orders.loc[j, "comb"],
                                                         new_orders.loc[j, "combHeight"], last_orders.loc[m, "ca"],
                                                         last_orders.loc[m, "cc"],
                                                         last_orders.loc[m, "yarns"], last_orders.loc[m, "comb"],
                                                         last_orders.loc[m, "combHeight"])
                    else:
                        setup_times.loc[i, j] = 60
        setup_dict[int(m)] = setup_times
    print(datetime.now(), " - End of setup times calculation.")
    return setup_dict


def knotting(ca_new, cc_new, yarns_new, comb_new, combHeight_new, ca_old, cc_old, yarns_old, comb_old, combHeight_old):
    if (ca_new == ca_old) and (
            cc_new == cc_old):
        setup = 0
    elif (ca_new == ca_old) or (
            yarns_new == yarns_old and (
            comb_new == comb_old)):
        # KNOTTING SETUP TYPE
        if yarns_new <= 3000:
            # EASY KNOTTING
            setup = 4 * 60
        elif yarns_new <= 5000:
            # MEDIUM KNOTTING
            setup = 4 * 60
        else:
            # HARD KNOTTING
            setup = 6 * 60
    else:
        # FULL SETUP
        if abs(combHeight_new - combHeight_old) <= 10:
            # EASY CHANGE
            setup = 4 * 60
        elif abs(combHeight_new - combHeight_old) <= 15:
            # MEDIUM CHANGE
            setup = 6 * 60
        else:
            # HARD CHANGE
            setup = 8 * 60

    return setup
