#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
from datetime import datetime
import simple_assignment
import sequence_dependent_tsp
import simple_working_resources
import simple_solution_check



def optimizer(orders, machines, setup, last_orders_fixed, number_of_workers, first_day_setup, atel_orders, extra_load):
    # assignment for large orders
    print(datetime.now(), " - Optimizer initialized.")
    max_assignments = len(orders.index) * len(machines.index)
    solutions = {'data': math.inf}
    best = {'data': math.inf}
    while max_assignments >= len(orders.index):
        available_looms = machines.copy(deep=True)
        last_orders = last_orders_fixed.copy()
        daily_setup_times = [0] * 5000
        daily_setup_times.insert(0, 3000 - first_day_setup)
        workers_list = [[math.inf] for i in range(number_of_workers)]
        quantities_assgn, process_time_assgn, order_dict, max_assignments = simple_assignment.simple_assignment(
            available_looms,
            orders,
            setup, max_assignments)
        # sequence for large aTSP
        loom_sequence = {}
        loom_load = {}

        for m in machines.index:
            loom_sequence[m] = []
            loom_load[m] = machines.loc[m, 'currentLoad']
            if len(order_dict[m]) > 0:
                loom_sequence[m], loom_load[m] = sequence_dependent_tsp.sequence_dependent_tsp(m, quantities_assgn,
                                                                                               process_time_assgn,
                                                                                               setup[m], orders)
            elif m in atel_orders.index:
                loom_sequence[m] = [machines.loc[m, "lastOrder"]]
        final_orders, workers_list, daily_setup_times, large_available_looms, new_last_orders = simple_working_resources.working_resources(
            setup,
            loom_sequence, available_looms,
            workers_list,
            daily_setup_times,
            process_time_assgn,
            orders,
            quantities_assgn, last_orders, number_of_workers, atel_orders, extra_load)

        solutions, best = simple_solution_check.solution_check(loom_sequence, final_orders, best, solutions,
                                                               machines['withATEL'], atel_orders)

        max_assignments -= 1
        # max_assignments = 0

    return solutions


def optimizer_no_splitting(orders, machines, setup, last_orders_fixed, number_of_workers, first_day_setup, atel_orders,
                           extra_load, start_date):
    # no splitting algorithm
    print(datetime.now(), " - Optimizer initialized.")
    max_assignments = len(orders.index) * len(machines.index)
    solutions = {'data': math.inf}
    best = {'data': math.inf}
    last_orders = last_orders_fixed.copy()
    daily_setup_times = [0] * 500
    daily_setup_times.insert(0, 3000 - first_day_setup)
    day_of_the_week = start_date.weekday() * 1440
    wknd_start = max(-0.1, 5 * 1440 + 360 - day_of_the_week)
    if wknd_start <= 360:
        wknd_start = -0.1
        wknd_end = 1440 * 7 + 360 - (day_of_the_week)
    else:
        wknd_end = wknd_start + 48 * 60
    workers_list = [[math.inf] for i in range(number_of_workers)]
    for i in range(0, 40):
        for j in range(number_of_workers):
            workers_list[j].extend([7 * 1440 * i + wknd_start, 7 * 1440 * i + wknd_end])
            workers_list[j].sort()
    quantities_assgn, process_time_assgn, order_dict, max_assignments = simple_assignment.simple_assignment_no_splitting(
        machines,
        orders,
        setup, max_assignments)
    # sequence for large aTSP
    loom_sequence = {}
    loom_load = {}

    for m in machines.index:
        loom_sequence[m] = []
        machines.loc[m,'opt_mkspn'] = machines.loc[m, 'currentLoad']
        if len(order_dict[m]) > 0:

            loom_sequence[m], machines.loc[m,'opt_mkspn'] = sequence_dependent_tsp.sequence_edd(m, machines.loc[m, 'loomSpeed'],
                                                                                 quantities_assgn,
                                                                                 process_time_assgn,
                                                                                 setup[m], orders, atel_orders,
                                                                                 order_dict[m])
            '''
            loom_sequence[m], machines.loc[m,'opt_mkspn'] = sequence_dependent_tsp.sequence_dependent_tsp(m,
                                                                                 quantities_assgn,
                                                                                 process_time_assgn,
                                                                                 setup[m],orders)
            '''
        elif m in atel_orders.index:
            loom_sequence[m] = [machines.loc[m, "lastOrder"]]
    final_orders, workers_list, daily_setup_times, large_available_looms, new_last_orders = simple_working_resources.working_resources(
        setup,
        loom_sequence, machines,
        workers_list,
        daily_setup_times,
        process_time_assgn,
        orders,
        quantities_assgn, last_orders, number_of_workers, atel_orders, extra_load)

    solutions, best = simple_solution_check.solution_check(loom_sequence, final_orders, best, solutions,
                                                           machines['withATEL'], atel_orders)

    return solutions
