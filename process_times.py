#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import numpy as np
import pandas as pd


def process_times_calculator(machines, orders):
    order_col = list(machines.index)
    order_row = list(orders.index)
    process_times = pd.DataFrame(columns=order_col, index=order_row)
    for i in order_row:
        for m in order_col:
            # process_time = (quantity * strokespermeter * ybf) /(loomspeed * loomefficiency)
            process_times.loc[i, m] = round(
                (orders.loc[i, "metersLeft"] * orders.loc[i, "strokesPerMeter"] * orders.loc[
                    i, "ybf"]) / (machines.loc[m, "loomSpeed"] * orders.loc[i, "loomEfficiency"]), 2)
    return process_times
