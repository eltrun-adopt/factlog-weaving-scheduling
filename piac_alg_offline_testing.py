#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import data_simple_preprocessing
import json
# import pandas as pd
import datetime
import data_simple_verification
import time
import optimizer
import setup_times_calc
import last_orders_calc


def opt():
    print(datetime.datetime.now(), " - Algorithm Start.")
    # print(" [x] Received %r" % body.decode())
    f = open('Piacenza_final_input.json', )
    input = json.load(f)
    input_data = input['data']
    print(datetime.datetime.now(), " - Data Received Successfully.")
    uuid = 5
    # print(f"Process UUID: {uuid}")
    # print(f["uuid"])

    # data validation
    problems = {}
    problems['data'] = {}
    try:
        data_ok, problems['data']['message'] = data_simple_verification.data_verification(input_data)

        if data_ok:
            # try:
            # data preprocessing
            first_day_setup = input_data['currentTotalSetupTime']
            looms_df, orders_df, extraLoad, zero_point, number_of_workers, atel_orders, start_date = data_simple_preprocessing.data_preprocessing(
                input_data)

            # find existing orders on looms - verify for ATEL

            last_orders, looms_df = last_orders_calc.last_orders_calculation(looms_df, atel_orders)

            setup_times = setup_times_calc.setup_times_calculator(orders_df, looms_df, last_orders)

            # optimizer

            solutions_dict = optimizer.optimizer_no_splitting(orders_df, looms_df, setup_times, last_orders,
                                                              number_of_workers, first_day_setup, atel_orders,
                                                              extraLoad, start_date)  # number of workers

            solutions_dict['data']['objectiveValues']['makespan'] = str(start_date + datetime.timedelta(
                minutes=round(solutions_dict['data']['objectiveValues']['makespan'], 0)))

            # output

            for name in solutions_dict.keys():
                for order in solutions_dict[name]['orders']:
                    for k in ['setupStartTime', 'setupEndTime', 'processStartTime', 'processEndTime',
                              'deliveryDate']:
                        try:
                            # print (order[k])
                            time_change = datetime.timedelta(minutes=order[k])
                            if k == 'deliveryDate':
                                order[k] = (zero_point + time_change).strftime("%m/%d/%Y")
                            else:
                                order[k] = (zero_point + time_change).strftime("%m/%d/%Y %H:%M:%S")
                        except TypeError:
                            break
            solutions_dict['uuid'] = uuid
            solutions_dict['produced_at'] = int(time.time() * 1000)
            # print(solutions_dict['makespan'])
            filename = "piac_out_1.json"
            with open(filename, 'w') as outfile:
                json.dump(solutions_dict, outfile)
            print(f"{datetime.datetime.now()}  - Algorithm End.")

        else:
            print(f"{datetime.datetime.now()}  - Incorrect Input - data failed validation stage.")
            problems['uuid'] = uuid
            problems['produced_at'] = int(time.time() * 1000)
            filename = "piac_out_problem.json"
            with open(filename, 'w') as outfile:
                json.dump(problems, outfile)
            print(f"{datetime.datetime.now()}  - Algorithm End.")
    except TypeError:
        print("OK")


'''
        except Exception as e:
            
            print(f"{datetime.datetime.now()}  - Algorithm killed - an error occurred, check your data.")
            print(str(e))
            error_message = {"message": str(e)}
            error_response = {"uuid": uuid, "data": error_message, "produced_at": int(time.time() * 1000)}
            filename = "piac_out_exception.json"
            with open(filename, 'w') as outfile:
                json.dump(error_response, outfile)
            print(f"{datetime.datetime.now()}  - Algorithm End.")
'''
opt()
