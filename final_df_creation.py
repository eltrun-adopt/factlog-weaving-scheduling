#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import numpy as np
import pandas as pd
from datetime import datetime


def final_df_creation(chainID, order_type, deliveryDate, m, setup_time, setup_start, process_time, targetMeters,
                      priorityWeight, extra_load,atel_start, flag_atel):
    # add to orders_final_df
    order_to_add = {}
    order_to_add['chainID'] = str(chainID)  # chainID
    order_to_add['partID'] = None  # partID - will fix at the end
    order_to_add['type'] = order_type  # type
    order_to_add['deliveryDate'] = float(deliveryDate)  # deliveryDate
    order_to_add['loomID'] = int(m)  # loomID
    order_to_add['setupTime'] = float(setup_time)  # setupTime
    order_to_add['setupStartTime'] = float(setup_start)  # setupStartTime
    order_to_add['setupEndTime'] = float(setup_start + setup_time)  # setupEndTime
    order_to_add['processingTime'] = round(float(process_time), 3)  # processingTime
    if flag_atel == 1:
        order_to_add['processStartTime'] = atel_start + extra_load
    else:
        order_to_add['processStartTime'] = float(setup_start + setup_time)  # processingStartTime
    order_to_add['processEndTime'] = float(setup_start + setup_time + process_time)  # processingEndTime
    order_to_add['energyCons'] = round(float(process_time / 60 * 7.5), 3)  # energyCons
    order_to_add['targetMeters'] = round(float(targetMeters), 3)  # targetMeters
    order_to_add['tardiness'] = round(
        float(max(setup_start + setup_time + process_time - max(deliveryDate, extra_load), 0)), 2)  # tardiness
    order_to_add['priorityWeight'] = int(priorityWeight)  # priorityWeight
    # print(order_to_add)

    return order_to_add
