#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import numpy as np
from datetime import datetime
import final_df_creation
import workers_availability


def working_resources(setup_times, loom_sequence, loom_df, workers_list, daily_setup_times, process_times,
                      orders, quantities, last_orders, number_of_workers, atel_orders, extra_load):
    print(f"{datetime.now()}  - Allocating Resources.")
    order_to_add = []
    earliest_worker_start = np.zeros((number_of_workers, 1))
    loom_df = loom_df.sort_values(['opt_mkspn'], ascending=[False])
    for m in loom_df.index:
        if len(loom_sequence[int(m)]) > 0:
            print(f"{datetime.now()}  - Allocating Resources for jobs on loomID: {m}")
            if loom_sequence[m][0] in list(atel_orders.chainType):
                # print(loom_sequence[m])
                chainID = atel_orders.loc[m, "chainID"]
                order_type = atel_orders.loc[m, "type"]
                setup_time = 0
                deliveryDate = atel_orders.loc[m, "deliveryDate"]
                targetMeters = atel_orders.loc[m, "metersLeft"]
                priorityWeight = atel_orders.loc[m, "priorityWeight"]
                process_time = round(
                    (atel_orders.loc[m, "metersLeft"] * atel_orders.loc[m, "strokesPerMt"] * atel_orders.loc[
                        m, "ybf"]) / (loom_df.loc[m, "loomSpeed"] * atel_orders.loc[m, "loomEfficiency"]), 2)
                loom_df.loc[m, "currentLoad"] = process_time + extra_load
                order_to_add.append(
                    final_df_creation.final_df_creation(chainID, order_type, deliveryDate, m, setup_time,
                                                        extra_load, process_time, targetMeters,
                                                        priorityWeight, extra_load, atel_orders.loc[m, 'atelStartDate'],
                                                        1))
            for i in range(1, len(loom_sequence[m])):
                new_order = loom_sequence[m][i]
                old_order = loom_sequence[m][i - 1]
                chainID = orders.loc[new_order, "chainID"]
                order_type = orders.loc[new_order, "type"]
                setup_time = setup_times[m].loc[old_order, new_order]
                deliveryDate = orders.loc[new_order, "deliveryDate"]
                targetMeters = quantities.loc[new_order, m]
                priorityWeight = orders.loc[new_order, "priorityWeight"]
                process_time = process_times.loc[new_order, m]
                if setup_time != 0:
                    for worker_resource in range(number_of_workers):
                        earliest_worker_start[worker_resource] = workers_availability.workers_availability(
                            daily_setup_times[:],
                            workers_list[worker_resource][:],
                            loom_df.loc[m, "currentLoad"],
                            setup_time, loom_df.loc[m, 'maintenanceStart'], loom_df.loc[m, 'maintenanceEnd'])
                    if math.isinf(np.min(earliest_worker_start)) or math.isnan(np.min(earliest_worker_start)):
                        if loom_df.loc[m, 'maintenanceStart'] is not None and loom_df.loc[
                            m, 'maintenanceEnd'] is not None:
                            if loom_df.loc[m, "currentLoad"] + setup_time > loom_df.loc[m, 'maintenanceStart'] and \
                                    loom_df.loc[m, "currentLoad"] < loom_df.loc[m, 'maintenanceEnd']:
                                chosen_worker = 0
                                earliest_worker_start[chosen_worker] = loom_df.loc[m, 'maintenanceEnd']
                            else:
                                chosen_worker = 0
                                earliest_worker_start[chosen_worker] = loom_df.loc[m, "currentLoad"]
                        else:
                            chosen_worker = 0
                            earliest_worker_start[chosen_worker] = loom_df.loc[m, "currentLoad"]
                    else:
                        chosen_worker = np.argmin(earliest_worker_start)
                        workers_list[int(chosen_worker)].extend(
                            [int(earliest_worker_start[chosen_worker]),
                             int(earliest_worker_start[chosen_worker] + setup_time)])
                        workers_list[int(chosen_worker)].sort()
                    daily_setup_times[math.floor((earliest_worker_start[chosen_worker]) / 1440)] += setup_time
                    print(
                        f"{datetime.now()}  - Found worker available for part of chainID: {chainID} on loomID: {m} on time: {int(earliest_worker_start[chosen_worker])}")
                else:
                    if loom_df.loc[m, 'maintenanceStart'] is not None and loom_df.loc[m, 'maintenanceEnd'] is not None:
                        if loom_df.loc[m, "currentLoad"] + setup_time > loom_df.loc[m, 'maintenanceStart'] and \
                                loom_df.loc[m, "currentLoad"] < loom_df.loc[m, 'maintenanceEnd']:
                            loom_df.loc[m, "currentLoad"] = loom_df.loc[m, 'maintenanceEnd']
                    chosen_worker = 0
                    earliest_worker_start[chosen_worker] = loom_df.loc[m, "currentLoad"]
                '''
                if earliest_worker_start[chosen_worker] + setup_time + process_time > loom_df.loc[
                    m, 'maintenanceStart'] and (
                        earliest_worker_start[chosen_worker] + setup_time + process_time < loom_df.loc[
                    m, 'maintenanceEnd'] or (earliest_worker_start[chosen_worker] + setup_time + process_time >
                                             loom_df.loc[m, 'maintenanceEnd'])):
                    process_time += loom_df.loc[m, 'maintenanceDuration']
                '''
                loom_df.loc[m, "currentLoad"] = earliest_worker_start[chosen_worker] + setup_time + process_time
                loom_df.loc[m, "lastOrder"] = new_order
                last_orders.loc[m] = orders.loc[
                    new_order, ["ca", "cc", "yarns", "chainID", "type", "comb", "combHeight"]]
                last_orders["chainType"] = last_orders.chainID.map(str) + last_orders.type
                setup_start = earliest_worker_start[chosen_worker]
                order_to_add.append(
                    final_df_creation.final_df_creation(chainID, order_type, deliveryDate, m, setup_time,
                                                        setup_start, process_time, targetMeters,
                                                        priorityWeight, extra_load, 'None', 0))
                print(f"{datetime.now()}  - Part of chainID: {chainID} on loomID: {m} done.")

    return order_to_add, workers_list, daily_setup_times, loom_df, last_orders
