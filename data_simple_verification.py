#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

from datetime import datetime

import pandas as pd


def data_verification(data_to_check):
    print(datetime.now(), " - Start of Data Validation.")

    problems = {}
    problems['startDate'] = ""
    problems['machines'] = []
    problems['orders'] = []
    problems['workGroupsPerShift'] = ""
    problems['currentTotalSetupTime'] = ""

    if isinstance(data_to_check, dict):
        problems['fileFormat'] = "Correct file format."

        # check startDate
        if 'startDate' in data_to_check.keys():
            start_date = data_to_check['startDate']
            try:
                start_date = datetime.strptime(start_date, '%m/%d/%Y %H:%M:%S')
            except Exception as e:
                problems[
                    "startDate"] = "Start Date has not valid format ", start_date, " - should be of type string (mm/dd/YY HH:MM:SS)."
        else:
            problems["startDate"] = "Start Date must be included - should be of type string (mm/dd/YY HH:MM:SS)."

        if 'currentTotalSetupTime' in data_to_check.keys():
            if not positive_int(data_to_check["currentTotalSetupTime"]):
                problems["currentTotalSetupTime"] = "Current total setup time ", data_to_check[
                    "currentTotalSetupTime"], " is not positive int."
        else:
            problems[
                "currentTotalSetupTime"] = "Current total setup time is not in keys - correct key is 'currentTotalSetupTime'."

        # Data validation goes on only if start Date is correct
        # print(len(problems['startDate']))
        if len(problems['startDate']) == 0:
            # workers check - existence of key, positive int
            if "workGroupsPerShift" in data_to_check.keys():
                workers = data_to_check["workGroupsPerShift"]
                if not positive_int(workers):
                    problems["workGroupsPerShift"] = 'Number of workers is ', data_to_check[
                        "workGroupsPerShift"], ' not positive int - check type.'
                # elif workers > 10:
                #    problems["workGroupsPerShift"] = 'Number of workers is probably incorrect.'
            else:
                problems[
                    "workGroupsPerShift"] = "Work Groups Per Shift are missing - correct key is 'workGroupsPerShift'."

            # looms check - existence of key, chainID positive int, speeds in speed range, maintenance Dates
            problem = {}
            if "looms" in data_to_check.keys():
                machines = data_to_check["looms"]
                # speeds = [280, 300, 400]
                if len(machines) > 0:
                    loom_ids = []
                    for loom in machines:
                        if "loomID" in loom.keys():
                            loom_ids.append(loom['loomID'])
                        else:
                            problem['loomID'] = 000000
                            problem['message'] = 'No ID for loom.'
                            problems['machines'].append(problem)
                            problem = {}
                    given_looms = pd.DataFrame(index=loom_ids,
                                               columns=['lastChainID', 'lastType', 'lastCa', 'lastCc', 'lastYarns',
                                                        'lastComb',
                                                        'lastCombHeight'])
                    for loom in machines:
                        problem = {}
                        if "loomID" in loom.keys():
                            if not (positive_int(loom['loomID'])):
                                problem['loomID'] = loom['loomID']
                                problem['message'] = 'LoomID is not positive int.'
                                problems['machines'].append(problem)
                                problem = {}
                            if 'loomSpeed' in loom.keys():
                                if not (positive_float(loom['loomSpeed'])) and (not positive_int(loom['loomSpeed'])):
                                    problem['loomID'] = loom['loomID']
                                    problem['message'] = 'Loom Speed is neither positive float nor positive int.'
                                    problems['machines'].append(problem)
                                    problem = {}
                            if "lastChainID" in loom.keys() and loom['lastChainID'] is not None:
                                if not positive_int(loom['lastChainID']):
                                    problem['loomID'] = loom['loomID']
                                    problem['message'] = 'lastChainID is not positive int.'
                                    problems['machines'].append(problem)
                                    problem = {}
                                else:
                                    given_looms.loc[loom['loomID'], 'lastChainID'] = loom['lastChainID']
                                if "lastType" not in loom.keys():
                                    problem['loomID'] = loom['loomID']
                                    problem['message'] = 'lastType is missing - correct key "lastType.'
                                    problems['machines'].append(problem)
                                    problem = {}
                                else:
                                    if not isinstance(loom['lastType'], str):
                                        problem['loomID'] = loom['loomID']
                                        problem['message'] = 'lastType is not string type.'
                                        problems['machines'].append(problem)
                                        problem = {}
                                    else:
                                        given_looms.loc[loom['loomID'], 'lastType'] = loom['lastType']

                                if 'lastComb' in loom.keys():
                                    if not positive_int(loom["lastComb"]):
                                        problem['loomID'] = loom['loomID']
                                        problem['message'] = 'lastComb is not positive int.'
                                        problems['machines'].append(problem)
                                        problem = {}
                                    else:
                                        given_looms.loc[loom['loomID'], 'lastComb'] = loom['lastComb']
                                else:
                                    problem['loomID'] = loom['loomID']
                                    problem['message'] = "lastComb is not in keys - correct key 'lastComb'."
                                    problems['machines'].append(problem)
                                    problem = {}
                                if 'lastCombHeight' in loom.keys():
                                    if not positive_float(loom["lastCombHeight"]) and not positive_int(
                                            loom["lastCombHeight"]):
                                        problem['loomID'] = loom['loomID']
                                        problem['message'] = 'lastCombHeight is not a positive number.'
                                        problems['machines'].append(problem)
                                        problem = {}
                                    else:
                                        given_looms.loc[loom['loomID'], 'lastCombHeight'] = loom['lastCombHeight']
                                else:
                                    problem['loomID'] = loom['loomID']
                                    problem['message'] = "lastCombHeight is not in keys - correct key 'lastCombHeight'."
                                    problems['machines'].append(problem)
                                    problem = {}
                                if 'lastYarns' in loom.keys():
                                    if not positive_int(loom["lastYarns"]):
                                        problem['loomID'] = loom['loomID']
                                        problem['message'] = 'lastYarns is not positive int.'
                                        problems['machines'].append(problem)
                                        problem = {}
                                    else:
                                        given_looms.loc[loom['loomID'], 'lastYarns'] = loom['lastYarns']
                                else:
                                    problem['loomID'] = loom['loomID']
                                    problem['message'] = "lastYarns is not in keys - correct key 'lastYarns'."
                                    problems['machines'].append(problem)
                                    problem = {}
                                if 'lastCa' in loom.keys():
                                    if not positive_int(loom["lastCa"]):
                                        problem['loomID'] = loom['loomID']
                                        problem['message'] = 'lastCa is not positive int.'
                                        problems['machines'].append(problem)
                                        problem = {}
                                    else:
                                        given_looms.loc[loom['loomID'], 'lastCa'] = loom['lastCa']
                                else:
                                    problem['loomID'] = loom['loomID']
                                    problem['message'] = "lastCa is not in keys - correct key 'lastCa'."
                                    problems['machines'].append(problem)
                                    problem = {}
                                if 'lastCc' in loom.keys():
                                    if not positive_int(loom["lastCc"]):
                                        problem['loomID'] = loom['loomID']
                                        problem['message'] = 'lastCc is not positive int.'
                                        problems['machines'].append(problem)
                                        problem = {}
                                    else:
                                        given_looms.loc[loom['loomID'], 'lastCc'] = loom['lastCc']
                                else:
                                    problem['loomID'] = loom['loomID']
                                    problem['message'] = "lastCc is not in keys - correct key 'lastCc'."
                                    problems['machines'].append(problem)
                                    problem = {}
                            if 'maintenanceStart' in loom.keys():
                                if loom['maintenanceStart'] is not None:
                                    if not check_dates(loom['maintenanceStart'], data_to_check['startDate']) and loom[
                                        'maintenanceStart'] != "Now":
                                        problem['loomID'] = loom['loomID']
                                        problem[
                                            'message'] = 'Maintenance Start has either wrong format or is precedent to schedule Start Date.'
                                        problems['machines'].append(problem)
                                        problem = {}
                            else:
                                loom['maintenanceStart'] = None
                            if 'maintenanceEnd' in loom.keys():
                                if loom['maintenanceEnd'] is not None:
                                    if (not check_dates(loom['maintenanceEnd'], loom['maintenanceStart'])) and loom[
                                        'maintenanceStart'] != "Now":
                                        problem['loomID'] = loom['loomID']
                                        problem[
                                            'message'] = 'Maintenance End has either wrong format or is precedent to Maintenance Start.'
                                        problems['machines'].append(problem)
                                    if not check_dates(loom['maintenanceEnd'], data_to_check['startDate']):
                                        problem['loomID'] = loom['loomID']
                                        problem[
                                            'message'] = 'Maintenance End is precedent to start date of the schedule.'
                                        problems['machines'].append(problem)
                            else:
                                loom['maintenanceEnd'] = None
                else:
                    problems['machines'] = "No looms have been sent."
            else:
                problems['machines'] = "No looms have been sent."
                problem = {}

            # check orders - chainID,
            if "orders" in data_to_check.keys():
                orders = data_to_check["orders"]
                status = ["ATEL", "LANC", "ORDI"]
                # chain_type = ["C", "F", "P"]
                if len(orders) > 0:
                    for order in orders:
                        problem = {}
                        # print (order)
                        if 'chainID' in order.keys():
                            if not positive_int(order["chainID"]):
                                problem['chainID'] = order['chainID']
                                problem['message'] = 'Chain ID is not positive int.'
                                problems['orders'].append(problem)
                                problem = {}
                            if 'targetMeters' in order.keys():
                                if not positive_float(order["targetMeters"]) and not positive_int(
                                        order["targetMeters"]):
                                    problem['chainID'] = order['chainID']
                                    problem['message'] = 'Quantity is not positive float or positive int.'
                                    problems['orders'].append(problem)
                                    problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "TargetMeters is not in keys - correct key 'targetMeters'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'deliveryDate' in order.keys():
                                try:
                                    delivery_date = datetime.strptime(order['deliveryDate'], '%m/%d/%Y')
                                except Exception as e:
                                    problem['chainID'] = order['chainID']
                                    problem[
                                        'message'] = "Delivery Date has not valid format - should be of type string (mm/dd/YY)."
                                    problems['orders'].append(problem)
                                    problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "Delivery Date is not in keys - correct key 'deliveryDate'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'ybf' in order.keys():
                                if not positive_float(order["ybf"]) and not positive_int(order["ybf"]):
                                    problem['chainID'] = order['chainID']
                                    problem['message'] = 'Ybf is not positive float or positive int.'
                                    problems['orders'].append(problem)
                                    problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "Ybf is not in keys - correct key 'ybf'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'comb' in order.keys():
                                if not positive_int(order["comb"]):
                                    problem['chainID'] = order['chainID']
                                    problem['message'] = 'Comb is not positive int.'
                                    problems['orders'].append(problem)
                                    problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "Comb is not in keys - correct key 'comb'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'combHeight' in order.keys():
                                if not positive_int(order["combHeight"]) and not positive_float(order["combHeight"]):
                                    problem['chainID'] = order['chainID']
                                    problem['message'] = 'Comb Height is not a positive number.'
                                    problems['orders'].append(problem)
                                    problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "CombHeight is not in keys - correct key 'combHeight'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'yarns' in order.keys():
                                if not positive_int(order["yarns"]):
                                    problem['chainID'] = order['chainID']
                                    problem['message'] = 'Yarns is not positive int.'
                                    problems['orders'].append(problem)
                                    problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "Yarns is not in keys - correct key 'yarns'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'incom' in order.keys():
                                if not positive_int(order["incom"]):
                                    problem['chainID'] = order['chainID']
                                    problem['message'] = 'Incom is not positive int.'
                                    problems['orders'].append(problem)
                                    problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "Incom is not in keys - correct key 'incom'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'priorityWeight' in order.keys():
                                if not positive_int(order["priorityWeight"] or order["priorityWeight"] <= 10):
                                    problem['chainID'] = order['chainID']
                                    problem['message'] = 'Priority weight is not positive int or is greater than 10.'
                                    problems['orders'].append(problem)
                                    problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "Priority Weight is not in keys - correct key 'priorityWeight'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'cc' in order.keys():
                                if not positive_int(order["cc"]):
                                    if not positive_int(order["ca"]):
                                        problem['chainID'] = order['chainID']
                                        problem['message'] = 'Chainability Code is not positive int.'
                                        problems['orders'].append(problem)
                                        problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "Chainability Code is not in keys - correct key 'cc'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'ca' in order.keys():
                                if not positive_int(order["ca"]):
                                    problem['chainID'] = order['chainID']
                                    problem['message'] = 'Annotability Code is not positive int or NULL.'
                                    problems['orders'].append(problem)
                                    problem = {}
                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "Annotability Code is not in keys - correct key 'ca'."
                                problems['orders'].append(problem)
                                problem = {}
                            if 'status' in order.keys():
                                if order["status"] not in status:
                                    problem['chainID'] = order['chainID']
                                    problem['message'] = 'Status is not ATEL, ORDI or LANC'
                                    problems['orders'].append(problem)
                                    problem = {}
                                else:
                                    if 'loomID' in order.keys() or order['status'] == "LANC" or order['status'] == "ORDI":
                                        if order['status'] == "ATEL":
                                            if order['loomID'] not in given_looms.index:
                                                problem['chainID'] = order['chainID']
                                                problem['message'] = 'loomID for ATEL order should be in given looms.'
                                                problems['orders'].append(problem)
                                                problem = {}
                                        else:
                                            if order['status'] == "ATEL":
                                                if 'atelStartDate' in order.keys():
                                                    if not check_dates(data_to_check['startDate'],
                                                                       order['atelStartDate']):
                                                        problem['chainID'] = order['chainID']
                                                        problem[
                                                            'message'] = f"atelStartDate for ATEL order must be precedent to start date."
                                                        problems['orders'].append(problem)
                                                        problem = {}
                                                else:
                                                    problem['chainID'] = order['chainID']
                                                    problem[
                                                        'message'] = f"atelStartDate is not in keys for ATEL order - correct key 'atelStartDate'."
                                                    problems['orders'].append(problem)
                                                    problem = {}
                                                if order['chainID'] != given_looms.loc[order['loomID'], 'lastChainID']:
                                                    problem['chainID'] = order['chainID']
                                                    problem[
                                                        'message'] = f"Different chainID for ATEL order and lastChainID on loomID: {order['loomID']}"
                                                    problems['orders'].append(problem)
                                                    problem = {}
                                                if order['yarns'] != given_looms.loc[order['loomID'], 'lastYarns']:
                                                    problem['chainID'] = order['chainID']
                                                    problem[
                                                        'message'] = f"Different yarns for ATEL order and lastYarns on loomID: {order['loomID']} ."
                                                    problems['orders'].append(problem)
                                                    problem = {}
                                                if order['comb'] != given_looms.loc[order['loomID'], 'lastComb']:
                                                    problem['chainID'] = order['chainID']
                                                    problem[
                                                        'message'] = f"Different comb for ATEL order and lastComb on loomID: {order['loomID']}"
                                                    problems['orders'].append(problem)
                                                    problem = {}
                                                if order['combHeight'] != given_looms.loc[
                                                    order['loomID'], 'lastCombHeight']:
                                                    problem['chainID'] = order['chainID']
                                                    problem[
                                                        'message'] = f"Different combHeight for ATEL order and lastCombHeight on loomID: {order['loomID']}"
                                                    problems['orders'].append(problem)
                                                    problem = {}
                                                if order['ca'] != given_looms.loc[order['loomID'], 'lastCa']:
                                                    problem['chainID'] = order['chainID']
                                                    problem[
                                                        'message'] = f"Different ca for ATEL order and lastCa on loomID: {order['loomID']}"
                                                    problems['orders'].append(problem)
                                                    problem = {}
                                                if order['cc'] != given_looms.loc[order['loomID'], 'lastCc']:
                                                    problem['chainID'] = order['chainID']
                                                    problem[
                                                        'message'] = f"Different cc for ATEL order and lastCc on loomID: {order['loomID']}"
                                                    problems['orders'].append(problem)
                                                    problem = {}
                                                if order['type'] != given_looms.loc[order['loomID'], 'lastType']:
                                                    problem['chainID'] = order['chainID']
                                                    problem[
                                                        'message'] = f"Different type for ATEL order and lastType on loomID: {order['loomID']}"
                                                    problems['orders'].append(problem)
                                                    problem = {}

                                    else:
                                        problem['chainID'] = order['chainID']
                                        problem['message'] = "loomID is not in keys - correct key 'loomID'."
                                        problems['orders'].append(problem)
                                        problem = {}
                                    if order['status'] == "ATEL":
                                        if 'kStrokes' in order.keys():
                                            if not positive_int(order["kStrokes"]) and not positive_float(
                                                    order["kStrokes"]):
                                                problem['chainID'] = order['chainID']
                                                problem['message'] = 'kstrokes for ATEL order is not a positive number.'
                                                problems['orders'].append(problem)
                                                problem = {}
                                        else:
                                            problem['chainID'] = order['chainID']
                                            problem['message'] = "kStrokes is not in keys - correct key 'kStrokes'."
                                            problems['orders'].append(problem)
                                            problem = {}

                            else:
                                problem['chainID'] = order['chainID']
                                problem['message'] = "Status is not in keys - correct key 'status'."
                                problems['orders'].append(problem)
                                problem = {}
                        else:
                            problem['chainID'] = 00000000
                            problem['message'] = "chainID is not in keys - correct key 'chainID'."
                            problems['orders'].append(problem)
                            problem = {}
                else:
                    problems['orders'] = "No orders have been sent."
            else:
                problems['orders'] = "No orders have been sent."
        # print(len(problems['machines']), len(problems['orders']), len(
        #    problems['workGroupsPerShift']), len(problems['startDate']), len(
        #    problems['currentTotalSetupTime']))
        if len(problems['machines']) > 0 or len(problems['orders']) > 0 or len(
                problems['workGroupsPerShift']) > 0 or len(problems['startDate']) or len(
            problems['currentTotalSetupTime']):
            data_ok = False
        else:
            data_ok = True
    else:
        problems["fileFormat"] = "Invalid json format."
        data_ok = False
    print(datetime.now(), " - End of Data Validation.")

    return data_ok, problems


def check_dates(date1, date2):
    correct_format = False
    if type(date1) == str and type(date2) == str:
        date1 = date1.replace(",", "")
        date2 = date2.replace(",", "")
        try:
            date1 = datetime.strptime(date1, '%m/%d/%Y %H:%M:%S')
            date2 = datetime.strptime(date2, '%m/%d/%Y %H:%M:%S')
            if date1 > date2:
                correct_format = True
        except Exception as e:
            correct_format = False
    return correct_format


def positive_int(a):
    correct_format = False
    if isinstance(a, int) and a > 0:
        correct_format = True
    return correct_format


def positive_float(a):
    correct_format = False
    if isinstance(a, float) and a > 0:
        correct_format = True
    return correct_format
