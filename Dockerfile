#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

FROM ubuntu:18.04 as buildoptimizer
ARG GRB_VERSION=9.1.1
ARG GRB_SHORT_VERSION=9.1
ENV RB_HOST $RB_HOST
ENV RB_PORT $RB_PORT
ENV RB_USER $RB_USER
ENV RB_PASS $RB_PASS

# install gurobi package and copy the files
WORKDIR /opt

RUN apt-get update \
    && apt-get install --no-install-recommends -y\
       ca-certificates  \
       wget \
    && update-ca-certificates \
    && wget -v https://packages.gurobi.com/${GRB_SHORT_VERSION}/gurobi${GRB_VERSION}_linux64.tar.gz \
    && tar -xvf gurobi${GRB_VERSION}_linux64.tar.gz  \
    && rm -f gurobi${GRB_VERSION}_linux64.tar.gz \
    && mv -f gurobi* gurobi \
    && rm -rf gurobi/linux64/docs

# After the file renaming, a clean image is build
FROM python:3.7-slim AS packageoptimizer

ARG GRB_VERSION=9.1.1

LABEL vendor="Gurobi"
LABEL version=${GRB_VERSION}

# update system and certificates
RUN apt-get update \
    && apt-get install --no-install-recommends -y\
       ca-certificates  \
       p7zip-full \
       zip \
    && update-ca-certificates \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/gurobi
COPY --from=buildoptimizer /opt/gurobi .

ENV GUROBI_HOME /opt/gurobi/linux64
ENV PATH $PATH:$GUROBI_HOME/bin
ENV LD_LIBRARY_PATH $GUROBI_HOME/lib

WORKDIR /opt/gurobi/linux64
#run the setup
RUN python setup.py install

COPY ./gurobi.lic ./
ENV GRB_LICENSE_FILE /opt/gurobi/linux64/gurobi.lic

WORKDIR /piacenza
COPY ./ ./
RUN pip install -r requirements.txt
ENTRYPOINT python piac_opt_alg_simple.py 1 $RB_HOST $RB_PORT $RB_USER $RB_PASS