# factlog: Weaving Scheduling


## Description
We solve a parallel machines scheduling problem (PMSP) with unrelated machines which is one
the most general among the variations. This is because, apart from unrelated machines, we 
allow for (i) sequence- and machine-dependent setup times: the setup time when jobj succeeds 
k is different than the time when k succeeds j and varies also per machine m; and (ii) setup
resource constraints: the number of setups that can be performed simultaneously on different machines
is restricted. 

We have applied this method to support scheduling decisions in a textile manufacturer located in northern Italy, 
which manufactures woolen fabrics for luxury clothing brands.

More details can also be found in:

<https://doi.org/10.1080/00207543.2022.2102948>

<https://link.springer.com/chapter/10.1007/978-3-030-85874-2_45>

## Authors
People who have contributed to this project are Stavros Vatikiotis and Dr. George Zois.

## Acknowledgement
This research was funded by the H2020 FACTLOG project, which has received funding
from the European Union’s Horizon 2020 programme under grant agreement No. 869951.
<https://www.factlog.eu/>

## License
factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.

factlog-weaving-scheduling is licensed under a
Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

You should have received a copy of the license along with this
work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.