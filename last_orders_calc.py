#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import numpy as np
import pandas as pd
from datetime import datetime


def last_orders_calculation(looms_df, atel_orders):
    print(datetime.now(), " - Start of last orders on looms calculation.")

    last_orders_df = pd.DataFrame(looms_df.index,
                                  columns=['ca', 'cc', 'yarns', 'chainID', 'type', 'loomID', 'comb',
                                           'combHeight']).set_index('loomID')
    atel_orders = atel_orders.reset_index(drop=False)
    for m in looms_df.index:
        last_orders_df.loc[m, 'ca'] = looms_df.loc[m, "lastCa"]
        last_orders_df.loc[m, 'cc'] = looms_df.loc[m, "lastCc"]
        last_orders_df.loc[m, 'yarns'] = looms_df.loc[m, "lastYarns"]
        if not math.isnan(looms_df.loc[m, "lastChainID"]):
            last_orders_df.loc[m, 'chainID'] = int(looms_df.loc[m, "lastChainID"])
        else:
            last_orders_df.loc[m, 'chainID'] = looms_df.loc[m, "lastChainID"]
        last_orders_df.loc[m, 'type'] = looms_df.loc[m, "lastType"]
        last_orders_df.loc[m, 'comb'] = looms_df.loc[m, "lastComb"]
        last_orders_df.loc[m, 'combHeight'] = looms_df.loc[m, "lastCombHeight"]
    last_orders_df["chainType"] = last_orders_df.chainID.map(str) + last_orders_df.type
    print(datetime.now(), " - End of last orders on looms calculation.")
    return last_orders_df, looms_df
