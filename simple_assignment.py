#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import numpy as np
from pyomo.environ import ConcreteModel, ConstraintList, Objective, Var, RangeSet, Binary, NonNegativeReals
from pyomo.opt import SolverFactory
from pyomo.repn.plugins.ampl.ampl_ import *
from pyomo.util.infeasible import log_infeasible_constraints
from pyomo.environ import value
import pandas as pd
from pyomo.opt import SolverStatus, TerminationCondition
from datetime import datetime


def simple_assignment(machines, jobs, setup_time, max_assignments):
    print(
        f"{datetime.now()}  - Creating assignment model for {max_assignments} max assignments and {len(jobs)} number of jobs.")
    order_dict = {}  # dictionary that will contain all orders with keys machine ids
    machine_ids = list(machines.index)
    for m in machine_ids:
        order_dict[m] = []  # initialize empty lists in dict
    job_ids = list(jobs.index)
    quantities_assgn = pd.DataFrame(0, columns=machine_ids, index=job_ids)  # quantity of each order on each loom
    process_time_assgn = pd.DataFrame(0, columns=machine_ids, index=job_ids)  # process time of each order on each loom

    # Initialize model

    model = ConcreteModel()
    model.jobs = job_ids
    model.machines = machine_ids
    model.Y = Var(model.jobs, model.machines, within=Binary, initialize=0.0)
    model.Q = Var(model.jobs, model.machines, within=NonNegativeReals, initialize=0.0)
    model.C_max = Var(within=NonNegativeReals, initialize=0.0)

    def obj_rule(model):

        sum_of_obj_rule = model.C_max

        return sum_of_obj_rule

    model.obj = Objective(rule=obj_rule)
    model.constraints = ConstraintList()

    # constraint for maximum number of splits
    constraint_sum = 0
    for i in job_ids:
        for m in machine_ids:
            constraint_sum += model.Y[i, m]
    model.constraints.add(
        (constraint_sum) <= max_assignments)
    # constraints for quantity

    for i in job_ids:
        model.constraints.add(
            (sum(model.Q[i, m] for m in machine_ids) == jobs.loc[i, "targetMeters"]))
        # print(sum(model.Q[i, m] for m in machine_ids) == jobs.loc[i, "targetMeters"])

    for i in job_ids:
        for m in machine_ids:
            model.constraints.add(model.Q[i, m] >= model.Y[i, m] * min(jobs.loc[i, 'targetMeters'], 50.0))
            model.constraints.add(model.Q[i, m] <= model.Y[i, m] * jobs.loc[i, 'targetMeters'])
            # print(model.Q[i, m] <= model.Y[i, m] * jobs.loc[i, 'targetMeters'])
            # print(model.Q[i, m] >= model.Y[i, m] * min(jobs.loc[i, 'targetMeters'], 50))

    # makespan constraint

    for m in machine_ids:
        constraint_sum = 0
        for i in job_ids:
            # (process time + min_setup) * Y_i
            constraint_sum += model.Q[i, m] * round(
                jobs.loc[i, 'strokesPerMt'] * jobs.loc[i, 'ybf'] / (
                        machines.loc[m, 'loomSpeed'] * jobs.loc[i, 'loomEfficiency']), 2) + \
                              setup_time[m].loc[setup_time[m][i] > 0, i].min() * model.Y[i, m]
        model.constraints.add(constraint_sum + machines.loc[m, "currentLoad"] <= model.C_max)
        # print(constraint_sum + machines.loc[m, "currentLoad"] <= model.C_max)

    print(datetime.now(), " - Assignment Model Created. ")
    opt = SolverFactory('gurobi')
    opt.options["MIPGap"] = 0.02
    opt.options['timelimit'] = 30

    ''' model.pprint()
    '''
    results = opt.solve(model)
    # results = opt.solve(model)
    log_infeasible_constraints(model)
    # print(results)
    max_assignments = 0
    print(datetime.now(), " - Assignment Model solved.")
    for v in model.component_objects(Var, active=True):
        # print("Variable component object", v)
        # print("Type of component object: ", str(type(v))[1:-1])  # Stripping <> for nbconvert
        varobject = getattr(model, str(v))
        # print("Type of object accessed via getattr: ", str(type(varobject))[1:-1])
        for index in varobject:

            if varobject[index].value != 0:
                # print("   ", index, varobject[index].value)
                if str(v) == "Q" and varobject[index].value > 0.1:
                    # print("   ", index, varobject[index].value)
                    quantities_assgn.loc[index[0], index[1]] = round(varobject[index].value, 2)
                    process_time_assgn.loc[index[0], index[1]] = round(
                        varobject[index].value * jobs.loc[index[0], 'strokesPerMt'] * jobs.loc[index[0], 'ybf'] / (
                                machines.loc[
                                    index[1], 'loomSpeed'] * jobs.loc[index[0], 'loomEfficiency']), 2)
                    order_dict[index[1]].append(index[0])

                if str(v) == "Y" and varobject[index].value > 0.9:
                    # print("   ", index, varobject[index].value)
                    max_assignments += 1
    print(f"{datetime.now()}  - Assignments in solution: {max_assignments}")
    return quantities_assgn, process_time_assgn, order_dict, max_assignments


def simple_assignment_no_splitting(machines, jobs, setup_time, max_assignments):
    print(f"{datetime.now()}  - Creating assignment model for {len(jobs)} number of jobs.")
    order_dict = {}  # dictionary that will contain all orders with keys machine ids
    machine_ids = list(machines.index)
    for m in machine_ids:
        order_dict[m] = []  # initialize empty lists in dict
    job_ids = list(jobs.index)
    quantities_assgn = pd.DataFrame(0, columns=machine_ids, index=job_ids)  # quantity of each order on each loom
    process_time_assgn = pd.DataFrame(0, columns=machine_ids, index=job_ids)  # process time of each order on each loom

    # Initialize model

    model = ConcreteModel()
    model.jobs = job_ids
    model.machines = machine_ids
    model.Y = Var(model.jobs, model.machines, within=Binary, initialize=0.0)
    # model.Q = Var(model.jobs, model.machines, within=NonNegativeReals, initialize=0.0)
    model.C_max = Var(within=NonNegativeReals, initialize=0.0)

    def obj_rule(model):

        sum_of_obj_rule = model.C_max

        return sum_of_obj_rule

    model.obj = Objective(rule=obj_rule)
    model.constraints = ConstraintList()

    for i in job_ids:
        model.constraints.add(
            (sum(model.Y[i, m] for m in machine_ids) == 1))
        # print(sum(model.Q[i, m] for m in machine_ids) == jobs.loc[i, "targetMeters"])
    # makespan constraint

    for m in machine_ids:
        constraint_sum = 0
        for i in job_ids:
            # (process time + min_setup) * Y_i
            constraint_sum += model.Y[i, m] * round(jobs.loc[i, 'targetMeters'] *
                                                    jobs.loc[i, 'strokesPerMt'] * jobs.loc[i, 'ybf'] / (
                                                            machines.loc[m, 'loomSpeed'] * jobs.loc[
                                                        i, 'loomEfficiency']), 2) + \
                              setup_time[m].loc[setup_time[m][i] > 0, i].min() * model.Y[i, m]
        model.constraints.add(constraint_sum + machines.loc[m, "currentLoad"] <= model.C_max)

    print(datetime.now(), " - Assignment Model Created. ")
    opt = SolverFactory('gurobi')
    opt.options["MIPGap"] = 0.005
    opt.options['timelimit'] = 30

    ''' model.pprint()
    '''
    results = opt.solve(model)
    # results = opt.solve(model)
    log_infeasible_constraints(model)
    # print(results)
    max_assignments = 0
    print(datetime.now(), " - Assignment Model solved.")
    for v in model.component_objects(Var, active=True):
        # print("Variable component object", v)
        # print("Type of component object: ", str(type(v))[1:-1])  # Stripping <> for nbconvert
        varobject = getattr(model, str(v))
        # print("Type of object accessed via getattr: ", str(type(varobject))[1:-1])
        for index in varobject:

            if varobject[index].value != 0:
                if str(v) == "Y" and varobject[index].value > 0.1:
                    # print("   ", index, varobject[index].value)
                    quantities_assgn.loc[index[0], index[1]] = jobs.loc[index[0], 'targetMeters']
                    process_time_assgn.loc[index[0], index[1]] = round(
                        jobs.loc[index[0], 'targetMeters'] * jobs.loc[index[0], 'strokesPerMt'] * jobs.loc[index[0], 'ybf'] / (
                                machines.loc[
                                    index[1], 'loomSpeed'] * jobs.loc[index[0], 'loomEfficiency']), 2)
                    order_dict[index[1]].append(index[0])

                if str(v) == "Y" and varobject[index].value > 0.9:
                    # print("   ", index, varobject[index].value)
                    max_assignments += 1
    print(f"{datetime.now()}  - Assignments in solution: {max_assignments}")
    return quantities_assgn, process_time_assgn, order_dict, max_assignments
