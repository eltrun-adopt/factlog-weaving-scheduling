#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math


def workers_availability(daily_setup_time, work, machines_time, setup_time, main_start, main_end):
    found_worker = False
    available_days = []
    if main_start is not None and main_end is not None:
        if machines_time + setup_time > main_start and machines_time < main_end:
            machines_time = main_end
    for i in range(len(daily_setup_time)):
        if daily_setup_time[i] + setup_time <= 3000 and (i + 1) * 1440 >= machines_time:
            available_days.append(i)
    machines_start_point = machines_time
    machines_best = machines_time
    counter = 0
    while not found_worker and counter <= 200:
        counter += 1
        machines_time += 0.0001
        work.append(machines_time)
        work.sort()
        # print(work)
        # print ("Workers list", workers_list)
        position_of_time = work.index(machines_time)
        # print(machines_time, setup_time)
        # print(work[position_of_time + 1])
        if position_of_time % 2 == 0:
            if machines_time + setup_time < work[position_of_time + 1]:
                machines_best = machines_time
            else:
                found = False
                while position_of_time + 3 < len(work) and not found:
                    if work[position_of_time + 2] + setup_time < work[position_of_time + 3]:
                        machines_best = work[position_of_time + 2] + 0.0001
                        found = True
                    else:
                        position_of_time += 2
        else:
            found = False
            while position_of_time + 2 < len(work) and not found:
                if work[position_of_time + 1] + setup_time < work[position_of_time + 2]:
                    machines_best = work[position_of_time + 1] + 0.0001
                    found = True
                else:
                    position_of_time += 2
        if math.floor(machines_best / 1440) in available_days:
            found_worker = True
        else:
            work.remove(machines_time)
            if len(available_days) > 1:
                available_days.pop(0)
                machines_time = available_days[0] * 1440
            else:
                break
    if not found_worker:
        machines_best = machines_start_point
    return machines_best - 0.0001
