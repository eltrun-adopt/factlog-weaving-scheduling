#    factlog-weaving-scheduling (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-scheduling is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import json
import random
import datetime
import numpy as np
import pandas as pd

pd.set_option('display.max_columns', None)


def delivery_date_func():
    # month = random.randint(6, 7)  # days of year
    month = 6
    day = random.randint(10, 30)
    hour = random.randint(0, 23)
    minute = random.randint(0, 59)
    return month, day, hour, minute


def maintenance_date_func():
    # month = random.randint(6, 7)  # days of year
    month = 6
    day = random.randint(3, 30)
    hour = random.randint(0, 23)
    minute = random.randint(0, 59)
    return month, day, hour, minute


# suppose start_date: 01/06/2021 (dd/mm/YYYY)
def json_input(number_of_chains):
    data = {}
    data["startDate"] = "05/02/2022 06:00:00"

    data['orders'] = []
    data['looms'] = []
    data['workGroupsPerShift'] = 3
    data['currentTotalSetupTime'] = 2500

    orders = pd.DataFrame(index=range(20),
                          columns=['status', 'chainID', 'loomID', 'targetMeters', 'kStrokes', 'deliveryDate', 'type',
                                   'priorityWeight',
                                   'fabricType', 'ca', 'cc', 'strokesPerMt', 'yarns', 'drawing', 'variant', 'incom',
                                   'comb', 'combHeight',
                                   'ybf', 'energyCons', 'atelStartDate'])

    for i in orders.index:
        if i < 6:
            orders.loc[i, 'status'] = "ATEL"
            orders.loc[i, 'atelStartDate'] = f"04/{random.randint(20, 25)}/2022 {random.randint(10, 20)}:00:00"
        else:
            orders.loc[i, 'status'] = "ORDI"
            orders.loc[i, 'atelStartDate'] = None
        orders.loc[i, 'chainID'] = (i + 1) * 2
        orders.loc[i, 'fabricType'] = random.randint(1000, 2000)
        orders.loc[i, 'ca'] = random.randint(1000, 2000)
        orders.loc[i, 'cc'] = random.randint(1000, 2000)
        orders.loc[i, 'strokesPerMt'] = random.randint(1000, 2000)
        orders.loc[i, 'yarns'] = random.randint(1, 20)
        orders.loc[i, 'drawing'] = random.randint(1, 20)
        orders.loc[i, 'variant'] = random.randint(1, 20)
        orders.loc[i, 'incom'] = random.randint(1, 20)
        orders.loc[i, 'comb'] = random.randint(100, 200)
        orders.loc[i, 'combHeight'] = round(random.random() * 1000, 2)
        orders.loc[i, 'loomID'] = (i + 1) ** 2
        orders.loc[i, 'targetMeters'] = round(random.random() * 1000, 2)
        orders.loc[i, 'type'] = random.choice(['P', 'C', 'F'])
        orders.loc[i, 'kStrokes'] = round(random.random() * 10, 2)
        orders.loc[i, 'deliveryDate'] = f"05/{random.randint(3, 10)}/2022"
        orders.loc[i, 'priorityWeight'] = random.randint(1, 10)
        orders.loc[i, 'ybf'] = 1.5
        orders.loc[i, 'energyCons'] = round(random.random() * 10, 2)
    if i > 16:
        orders.loc[i, :] = orders.loc[i - 1, :]
        orders.loc[i, 'loomID'] = (i + 1) ** 2
        orders.loc[i, 'targetMeters'] = round(random.random() * 1000, 2)
        orders.loc[i, 'kStrokes'] = round(random.random() * 10, 2)
        orders.loc[i, 'energyCons'] = round(random.random() * 10, 2)
    print(orders)
    number_of_fabrics = 15  # number of fabric codes

    # loom_id = random.sample(range(1, 100), number_of_looms)  # create random loom_id
    loom_id = list(orders.loomID.unique())
    speeds = [280, 300, 400]
    for m in loom_id:
        loom = {}
        loom["loomID"] = m
        loom["loomSpeed"] = random.choice(speeds)
        ateldf = orders.loc[orders['status'] == "ATEL"]
        if m in ateldf.loomID.unique():
            loomind = ateldf.index[ateldf['loomID'] == m][0]
            loom["lastChainID"] = ateldf.loc[loomind, 'chainID']
            loom["lastType"] = ateldf.loc[loomind, 'type']
            loom["lastCa"] = ateldf.loc[loomind, 'ca']
            loom["lastCc"] = ateldf.loc[loomind, 'cc']
            loom["lastYarns"] = ateldf.loc[loomind, 'yarns']
            loom["lastComb"] = ateldf.loc[loomind, 'comb']
            loom["lastCombHeight"] = ateldf.loc[loomind, 'combHeight']
        else:
            if random.random() < 1:
                loom["lastChainID"] = random.randint(10000, 200000)
                loom["lastType"] = random.choice(["P", "F", "C"])
                loom["lastCa"] = random.randint(200, 500)
                loom["lastCc"] = random.randint(300, 500)
                loom["lastYarns"] = random.randint(200, 6000)
                loom["lastComb"] = random.randint(100, 4000)
                loom["lastCombHeight"] = random.randint(100, 200)
            else:
                loom["lastChainID"] = None
                loom["lastType"] = None
                loom["lastCa"] = None
                loom["lastCc"] = None
                loom["lastYarns"] = None
                loom["lastComb"] = None
                loom["lastCombHeight"] = None
        month, day, hour, minute = maintenance_date_func()
        if random.randint(1, 100) > 90:
            maintenance_start = datetime.datetime(2022, 5, 3, hour, minute)
            maintenance_duration = random.randint(1000, 10000)
            maintenance_end = maintenance_start + datetime.timedelta(minutes=maintenance_duration)
            loom["maintenanceStart"] = maintenance_start.strftime("%m/%d/%Y %H:%M:%S")
            loom["maintenanceEnd"] = maintenance_end.strftime("%m/%d/%Y %H:%M:%S")
        elif random.randint(1, 100) > 80:
            loom["maintenanceStart"] = "Now"
            loom["maintenanceEnd"] = None
        elif random.randint(1, 100) > 70:
            loom["maintenanceStart"] = "Now"
            maintenance_duration = random.randint(1000, 10000)
            maintenance_end = datetime.datetime(2022, 5, 3, 6, 0) + datetime.timedelta(minutes=maintenance_duration)
            loom["maintenanceEnd"] = maintenance_end.strftime("%m/%d/%Y %H:%M:%S")
        else:
            loom["maintenanceStart"] = None
            loom["maintenanceEnd"] = None

        # loom["maintenanceDuration"] = maintenance_duration
        data['looms'].append(loom)
    for i in orders.index:
        new_order = {}
        new_order["status"] = orders.loc[i, 'status']
        new_order["chainID"] = orders.loc[i, 'chainID']
        # new_order["partID"] = order[2]
        new_order["loomID"] = orders.loc[i, 'loomID']
        new_order["targetMeters"] = orders.loc[i, 'targetMeters']
        new_order["kStrokes"] = orders.loc[i, 'kStrokes']
        new_order["deliveryDate"] = orders.loc[i, 'deliveryDate']
        new_order["type"] = orders.loc[i, 'type']
        new_order["priorityWeight"] = orders.loc[i, 'priorityWeight']
        new_order["fabricType"] = orders.loc[i, 'fabricType']
        new_order["ca"] = orders.loc[i, 'ca']
        new_order["cc"] = orders.loc[i, 'cc']
        new_order["strokesPerMt"] = orders.loc[i, 'strokesPerMt']
        new_order["yarns"] = orders.loc[i, 'yarns']
        new_order["drawing"] = orders.loc[i, 'drawing']
        new_order["variant"] = orders.loc[i, 'variant']
        new_order["incom"] = orders.loc[i, 'incom']
        new_order["comb"] = orders.loc[i, 'comb']
        new_order["combHeight"] = orders.loc[i, 'combHeight']
        new_order["ybf"] = orders.loc[i, 'ybf']
        new_order["energyCons"] = orders.loc[i, 'energyCons']
        new_order['atelStartDate'] = orders.loc[i, 'atelStartDate']

        # print(new_order)
        data['orders'].append(new_order)
        # print(order)


    dataout = {}
    dataout['data'] = data
    print(dataout)
    filename = f"Piacenza_final_input.json"
    with open(filename, 'w') as outfile:
        json.dump(dataout, outfile)
    return filename


json_input(40)
